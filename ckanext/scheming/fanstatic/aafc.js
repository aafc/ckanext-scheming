function getToday() {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    return now.getFullYear() + "-" + (month) + "-" + (day);
}

var current_path = window.location.pathname;

var gOdrc = "";
var gEl = "";
var gBi = "";

if (current_path.includes("/fr/")) {
    console.log("French versin");
    gOdrc = "#group-Informations-pour-la-diffusion-sur-le-portail-de-données-ouvertes";
    gEl = "#group-Licence";
    gBi = "#group-Informations-de-base";
} else {
    console.log("English version");
    gOdrc = "#group-Open-Data-Portal-Release-Information";
    gEl = "#group-External-Licence";
    gBi = "#group-Basic-Information";
}

// Dynamically switch metadata form when switching to Internal (partner) publication type
function switchToInternal() {
    if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)) {

        $('span').removeClass('input-group-btn');
        $('span').removeClass('glyphicon glyphicon-calendar');
        $('a').removeClass('btn btn-default picker-toggle');
        console.log("Please dont use IE.");
    }
    $(gOdrc).addClass("hidden");
    //Default to "blank"
    //$("select#field-access_to_information").val("false");
    $("select#field-license_id").val("ca-ogl-lgo");
    $("#field-subject option[value='other']").prop("selected", true);
    $('#field-private').trigger('change', true);
    $(gBi).toggle().toggle();
    if ($("select#field-elegible_for_release").val() == "false")
        if ($("select#field-ineligibility_reason").val() == "")
            $("select#field-ineligibility_reason").val("ownership");
        //eligibility and reason hidden in Internal
    $("select#field-elegible_for_release").parent().parent().hide();
    $("select#field-ineligibility_reason").parent().parent().hide();


    if ($("select#field-elegible_for_release").val() == "")
        $("select#field-elegible_for_release").val("false");
    if ($("select#field-imso_approval").val() == "")
        $("select#field-imso_approval").val("false");
    if ($("select#field-ready_to_publish").val() == "" || $("select#field-ready_to_publish").val() == null)
        $("select#field-ready_to_publish").val("false");
    if ($("select#field-ready_to_republish").val() == "" || $("select#field-ready_to_republish").val() == null)
        $("select#field-ready_to_republish").val("false");
    if ($("select#field-ineligibility_reason").val() == "")
        $("select#field-ineligibility_reason").val("privacy");

    if ($("select#field-access_to_information").val() == "" || $("select#field-access_to_information").val() == null)
        $("select#field-access_to_information").val("true");
    if ($("select#field-authority_to_release").val() == "")
        $("select#field-authority_to_release").val("true");
    if ($("select#field-formats").val() == "")
        $("select#field-formats").val("true");
    if ($("select#field-privacy").val() == "")
        $("select#field-privacy").val("true");
    if ($("select#field-official_language").val() == "")
        $("select#field-official_language").val("false");
    if ($("select#field-security").val() == "")
        $("select#field-security").val("true");
    if ($("select#field-other").val() == "")
        $("select#field-other").val("true");
    if (current_path.endsWith("new"))
        $("#field-data_steward_email").val("");
    $("#field-program_page_url-en").val("");
    $("#field-program_page_url-fr").val("");
    $("#field-organizations").parent().parent().parent().hide();
    var default_org = $("#field-organizations").prop("options")[1].value;
    $("#field-organizations").val(default_org);
    check_role_to_e();
    console.log("switch to intenral");
}

function setBoolToEmpty() {
    $("#field-access_to_information").val("");
    $("#field-authority_to_release").val("");
    $("#field-formats").val("");
    $("#field-privacy").val("");
    $("#field-official_language").val("");
    $("#field-security").val("");
    $("#field-other").val("");

    $("select#field-imso_approval").val("");
}
//Just for new
function switchToInternalNew() {
    check_role_to_g();
    $('#field-private').prop('selectedIndex', 0);
}

function switchToOpenGovernmentNew() {
    $('#field-private').prop('selectedIndex', 0);
}

// Dynamically switch metadata form when switching to Open Government (living labs) publication type
function switchToOpenGovernment() {
    switchToOpenGovernmentWithParam(false);
}

function switchToOpenGovernmentWithParam(internalToGovernment) {
    if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)) {
        $('span').removeClass('input-group-btn');
        $('span').removeClass('glyphicon glyphicon-calendar');
        $('a').removeClass('btn btn-default picker-toggle');

        console.log("Please dont use IE.");
    }
    check_role_to_g();
    $(gOdrc).removeClass("hidden");

    //eligibility and reason shown in Internal
    $("select#field-elegible_for_release").parent().parent().hide();
    $("select#field-ineligibility_reason").parent().parent().hide();

    if ($("select#field-elegible_for_release").val() == "")
        $("select#field-elegible_for_release").val("false");
    if ($("select#field-ineligibility_reason").val() == "")
        $("select#field-ineligibility_reason").val("privacy");
    //$("select#field-access_to_information").val("false");
    //$("select#field-license_id").val("");
    $("select#field-license_id_ext").val("ca-ogl-lgo");
    $("select#field-license_id option[value='aafc-dsa']").hide();
    $('#field-private').trigger('change', true);
    $(gBi).toggle().toggle();
    if ($("#field-data_steward_email").val() == "")
        $("#field-data_steward_email").val("aafc.datalivinglaboratoriesdonneeslaboratoiresvivants.aac@agr.gc.ca");
    $("#field-program_page_url-en").val("https://agriculture.canada.ca/en/scientific-collaboration-and-research-agriculture/living-laboratories-initiative");
    $("#field-program_page_url-fr").val("https://agriculture.canada.ca/fr/collaboration-scientifique-agriculture/initiative-laboratoires-vivants");
    $("#field-organizations").parent().parent().parent().show();

    $("#field-ready_to_publish").val("false");
    $("#field-ready_to_republish").val("false");
    if (internalToGovernment)
        setBoolToEmpty();

}

// Set default value and place holder for Date Published field
if ($("#field-date_published").val() == "") {
    var date = getToday();
    $("#field-date_published").val(date);
    $("#field-date_published").attr("placeholder", date);
}

function hideAndDefaultFieldsNotUsedFromRegistry() {
    $("#field-audience").parent().parent().hide();
    $("select#field-procured_data").val("false");
    //$("#field-procured_data").parent().parent().hide();
    $("select#field-drf_core_responsibilities").val("science_and_innovation");
    $("select#field-drf_core_responsibilities").trigger('change', true);
    $("select#field-drf_program_inventory").val("foundational_science_and_research");
    $("#field-drf_core_responsibilities").parent().parent().hide();
    $("#field-drf_program_inventory").parent().parent().hide();
    //$("#field-procured_data_organization_name").parent().parent().hide();
    $("select#field-authoritative_source").val("false");
    $("#field-authoritative_source").parent().parent().hide();
    $("#field-aafc_organizations").parent().parent().parent().hide();
    $("#field-aafc_owner_org").val("Agriculture and Agri-Food Canada");
    $("#field-aafc_owner_org").parent().parent().hide();
    $("#field-spatial").parent().parent().hide();
    $("#field-data_series_name-en").parent().parent().hide();
    $("#field-data_series_name-fr").parent().parent().hide();
    $("#field-data_series_issue_identification-en").parent().parent().hide();
    $("#field-data_series_issue_identification-fr").parent().parent().hide();
    $("#field-apm_departmental_identifier").parent().parent().hide();
    $("#field-restrictions").parent().parent().hide();
    $("#field-aafc_is_harvested").parent().parent().hide();
    $("#field-aafc_sector").parent().parent().hide();
    $("#field-aafc_sector").val("other");
    $("#field-data_released").parent().parent().hide();
    $("#field-open_government_portal_record_e").parent().parent().hide();
    $("#field-open_government_portal_record_f").parent().parent().hide();

    //$("#field-aafc_subject").parent().parent().hide();
    $("#field-subject").parent().parent().hide();
    $("#field-elegible_for_release").parent().parent().hide();
    $("#field-elegible_for_release").parent().parent().hide();
    $("#field-ineligibility_reason").parent().parent().hide();
    $("#field-license_id").parent().parent().hide();
    $("#field-ready_to_publish").parent().parent().hide();
    $("#field-ready_to_republish").parent().parent().hide();

    console.log("hideFieldsNotUsedFromRegistry COMPLETE");

}

function turnP() {
    if (pub_type == "open_government")
        switchToOpenGovernment();

    else
        switchToInternal();

}

function whats() {
    turnP();
}

function conditionalShow() {
    var selected = $(this).val();
    if (selected == "internal") {
        //$("#group-Open-Data-Release-Criteria").addClass("hidden");
        switchToInternal();

    } else {
        //$("#group-Open-Data-Release-Criteria").removeClass("hidden");
        //switchToOpenGovernment();
        switchToOpenGovernmentWithParam(true);
    }
}
$('#field-publication').change(conditionalShow);

$('#ctrl-Optional-Dataset-Information').addClass("collapsed");

$("input[alttype='date']").focus(function(e) {
    e.target.type = 'date';
});

$("input[alttype='date']").blur(function(e) {
    e.target.type = 'text';
});

$("input[alttype='date']").on("input", function(e) {
    e.target.type = 'text';
    e.target.blur();
});

var visibility_options = $("select#field-private>option");
var option_public = $("select#field-private>option[value='False']")[0];


function check_role(org_id, call_back) {
    var org_path = window.location.origin + "/api/3/action/organization_show?id=" + org_id;
    var role = "internal"; //default as lowest
    $.ajax({
        url: org_path,
        type: "GET",
        success: function(data) {
            //get current user
            var user_url = $("[href*='/user']")[0].href;
            var user_id = user_url.substring(user_url.lastIndexOf('/') + 1);
            var org_users = data.result.users;
            org_users.forEach(user => {
                if (user.name == user_id)
                    role = user.capacity;
            });
            call_back(role);

        }
    })

}

//check role when switch to "Government" publication type
function check_role_to_g() {
    var org_id = $("#field-organizations").val();
    check_role(org_id, function(r) {
        if ($("select#field-private>option[value='False']").length == 1)
            $("select#field-private>option[value='False']")[0].remove();
        if (r == "admin") //When the role of the org is admin then public is available
            $("select#field-private").prepend(new Option(option_public.text, option_public.value));
        console.log("switchTo Governme:" + r);
    });
}

function check_role_to_e() {
    var org_id = $("#field-organizations").val();
    check_role(org_id, function(r) {
        if ($("select#field-private>option[value='False']").length == 1)
            $("select#field-private>option[value='False']")[0].remove();
        if (r == "admin") //When the role of the org is admin then public is available
            $("select#field-private").prepend(new Option(option_public.text, option_public.value));
        console.log("switchTo External:" + r);
    });
}
//restore both private and public in dropdown menu
function restore_visibility() {
    if ($("select#field-private>option[value='False']").length == 0)
        $("select#field-private").append(new Option(option_public.text, option_public.value));
}

$("#field-organizations").on("change", function(e) {
    var org_id = e.target.value;
    //$("#selected_org").val(org_id);
    check_role(org_id, function(r) {
        if ($("select#field-private>option[value='False']").length == 1)
            $("select#field-private>option[value='False']")[0].remove();
        if (r == "admin") //When the role of the org is admin then public is available
            $("select#field-private").prepend(new Option(option_public.text, option_public.value));
        console.log(r);
    });

    /*    var org_path = window.location.origin + "/api/3/action/organization_show?id=" + org_id;
        $.ajax( {
            url : org_path,
            type : "GET",
            success : function(data) {
    	    //remove private option first
    	    if ($("select#field-private>option[value='False']").length == 1)
    		$("select#field-private>option[value='False']")[0].remove();
                //get current user
                var user_url = $("[href*='/user']")[0].href;
                var user_id = user_url.substring(user_url.lastIndexOf('/') + 1);
                var org_users = data.result.users;
                org_users.forEach(user => {
                   if(user.name == user_id && user.capacity == "admin") //When the role of the org is admin then public is available
    	          $("select#field-private").prepend(new Option(option_public.text,option_public.value));
                });
            }
        }) */
});






var pub_type = $("#field-publication").val();
var current_path = window.location.pathname;


if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(search, this_len) {
        if (this_len === undefined || this_len > this.length) {
            this_len = this.length;
        }
        return this.substring(this_len - search.length, this_len) === search;
    };
}


if (current_path.endsWith("new") || current_path.includes("/edit/")) {
    //if new...default things for new
    if (current_path.endsWith("new")) {
        if (pub_type == "open_government")
            switchToOpenGovernmentNew();
        else
            switchToInternalNew();
    }
    //new or edit
    if (pub_type == "open_government")
        switchToOpenGovernment();
    else
        switchToInternal();

    hideAndDefaultFieldsNotUsedFromRegistry();
    $("#field-theme-general").remove();
}

function isCrappyIE() {
    var ua = window.navigator.userAgent;
    var crappyIE = false;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) { // IE 10 or older => return version number        
        crappyIE = true;
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) { // IE 11 => return version number        
        crappyIE = true;
    }
    return crappyIE;
}


function organizationChange(e) {
    console.log("organization changed")
}