/***
for selection of dropdown  
 Program Alignment Architecture to DRF Core Responsibilities
change the option of dropdown
 DRF Program Inventory

*/

var igl_reasons=[];
var dic = {}
$('ul.mdpd').each( 
    function(index) { 
        var chidren = Array.prototype.slice.call(this.childNodes,0);
        var parent = this.id;
        dic[parent] = [];
        chidren.forEach ( function (data){
           var id = data.id;
           var label = data.innerText;
	   var newItem = {};
	   newItem[id] = label;
           dic[parent].push(newItem);
        });
    }

 );

var paa2drfData = dic;

//build ineligilibity reasons
$("select#field-ineligibility_reason option").each(
	function(){
		var val =  $(this).val();
		var text = $(this).text();
		if( val != '' && val != 'na')
		{
			item = {}
			item[val] = text
			igl_reasons.push(item);
		}
	}
)

if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(search, this_len) {
        if (this_len === undefined || this_len > this.length) {
            this_len = this.length;
        }
        return this.substring(this_len - search.length, this_len) === search;
    };
}
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
      position = position || 0;
      return this.indexOf(searchString, position) === position;
    };
  }
//initialize the child dropdown based on current UI is new dataset or edit dataset
var current = window.location.pathname;
if (current.endsWith("new")){
	$('#field-drf_program_inventory').empty();
} else if (current.startsWith('/dataset/edit/')){
	var savedParentValue= $('#field-drf_core_responsibilities').val()
	var savedChildValue = $('#field-drf_program_inventory').val()
	$('#field-drf_program_inventory').empty();
	buildChildrenDropdown(savedParentValue);
	$('#field-drf_program_inventory').val(savedChildValue)
    console.log("Edit!:" + current );
    console.log("Edit,saved child:" + savedChildValue + ",parent:"+ savedParentValue);
}
var myOptions = {
    "Value 1" : "Text 1",
    "Value 2" : "Text 2",
    "Value 3" : "Text 3"
}

// Helper function to build DRF progam inventory dropdown menu
function buildChildrenDropdown(parent) {
    $("#field-drf_program_inventory").empty();
    var children = paa2drfData[parent];
    children.forEach ( function(item) {
        var val = Object.keys(item)[0];
        var label = item[val];
        $("#field-drf_program_inventory").append(
            $('<option></option>').val(val).html(label)
        );
    })
}

// Build DRF program inventory when core responsibilities change
$("#field-drf_core_responsibilities").change(
    function(){
        var parent = $("#field-drf_core_responsibilities").val();
	    buildChildrenDropdown(parent);
    }
);

// Build DRF program inventory on page load
$( function () {
    var parent = $("#field-drf_core_responsibilities").val();
    if (parent != "") {
        buildChildrenDropdown(parent);
    }
});

//Ineligibility Reasons
//hide N/A option initially when page renders
$(function(){
  $("select#field-ineligibility_reason option[value='na']").hide();
});
//dynamically change the ineligibility reasons
$("select#field-elegible_for_release").change(function(){
    var currentSel = $(this).children("option:selected").val();
    $("#field-ineligibility_reason").empty();
    if (currentSel == "false"){
      igl_reasons.forEach(
        function(item){
            var val = Object.keys(item)[0];
            var label = item[val];
            $("#field-ineligibility_reason").append(
                $('<option></option>').val(val).html(label)
            );
        }
       )
    }
    else{
      $("#field-ineligibility_reason").append(
                $('<option></option>').val("na").html("N/A")
      );    	       
    }
  }
)

//dynamically change procured data organization name
$("#field-procured_data").change(
    function(){
        var selection = $(this).children("option:selected").val();
        if (selection == "false") {
                $("#field-procured_data_organization_name").val("N/A");
                $('#field-procured_data_organization_name').attr('readonly', true);
        } else {
                $("#field-procured_data_organization_name").val("");
                $('#field-procured_data_organization_name').attr('readonly', false);
    }
});
